<?php

namespace App;

interface ContainerCallbackInterface
{
    public function execute(ContainerInterface $container, array $arguments = []): mixed;
}