<?php

namespace App;

use App\Interfaces\Command;
use App\Interfaces\CommandExceptionStrategy;
use App\Interfaces\Queue;
use Throwable;

class ExceptionHandler
{
    private const DEFAULT = 'default';
    private array $strategies = [];

    /**
     * @param Queue                      $queue
     * @param CommandExceptionStrategy[] $strategies
     */
    public function __construct(
        private Queue $queue,
        array         $strategies
    ) {
        foreach ($strategies as $strategy) {
            $this->addStrategy($strategy);
        }
    }

    public function handle(Command $command, Throwable $throwable): void
    {
        /**@var CommandExceptionStrategy|null */
        $commandStrategies = $this->strategies[$command::class] ?? throw $throwable;
        $strategy = $commandStrategies[$throwable::class] ?? $commandStrategies[self::DEFAULT];
        $this->queue->add($strategy->create($command, $throwable));
    }

    private function addStrategy(CommandExceptionStrategy $strategy): void
    {
        $this->strategies[$strategy->supportCommand()][$strategy->supportException() ?? self::DEFAULT] = $strategy;

    }

}