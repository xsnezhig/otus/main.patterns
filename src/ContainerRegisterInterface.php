<?php

namespace App;

interface ContainerRegisterInterface
{
    public function register(string $id, ContainerCallbackInterface|array $options = []): void;
}