<?php

namespace App;

use App\Commands\Collection;
use App\Interfaces\Command;
use App\Interfaces\ExceptionHandler;
use App\Interfaces\Queue as QueueInterface;
use App\Interfaces\QueueState;
use App\Interfaces\QueueStateAccess;
use App\QueueState\CommonState;

class Queue implements QueueInterface, QueueStateAccess
{

    public function __construct(
        private readonly ExceptionHandler $handler,
        private readonly Collection       $collection = new Collection(),
        private QueueState|null           $state = new CommonState()
    )
    {
    }

    public function run(): void
    {

        while ($command = $this->collection->current()) {
            if (!$this->state) {
                $this->collection->clear();
                break;
            }
            try {
                $this->collection->remove($this->collection->key());
                $this->collection->next();
                $this->state->execute($command);
            } catch (\Throwable $e) {
                $this->handler->handle($command, $e);
            }
        }
    }

    public function add(Command $command): void
    {
        $this->collection->add($command);
    }

    public function setState(?QueueState $state): void
    {
        $this->state = $state;
    }
}