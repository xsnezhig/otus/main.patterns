<?php

namespace App;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    public function make($id, array $options = []): mixed;
}