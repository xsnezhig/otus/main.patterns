<?php

namespace App\Exceptions;

use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;

class NotSupportedTypeException extends \Exception
{
}