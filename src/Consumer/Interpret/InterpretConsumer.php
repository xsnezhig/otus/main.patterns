<?php

namespace App\Consumer\Interpret;

use App\Commands\Interpret;
use App\Consumer\Interpret\Input\Message;
use App\ContainerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class InterpretConsumer
{
    public function __construct(
        private readonly ContainerInterface $container
    )
    {
    }

    public function execute(AMQPMessage $msg): void
    {
        $message = Message::createFromBody($msg->body);
        $this->container->make(Interpret::class, [
            'gameId' => $message->getGameId(),
            'objectId' => $message->getObjectId(),
            'operationId' => $message->getOperationId(),
            'operationArgs' => $message->getOperationArgs()
        ])->execute();
    }
}