<?php

namespace App\Consumer\Interpret\Input;

class Message
{
    private int $gameId;
    private int $objectId;
    private string $operationId;
    private array $operationArgs;

    private function __construct()
    {

    }

    public static function createFromBody(string $body): self
    {
        $data = json_decode($body, true);
        $msg = new self();
        $msg->gameId = $data['gameId'];
        $msg->objectId = $data['objectId'];
        $msg->operationId = $data['operationId'];
        $msg->operationArgs = $data['operationArgs'];
        return $msg;
    }

    /**
     * @return int
     */
    public function getGameId(): int
    {
        return $this->gameId;
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return string
     */
    public function getOperationId(): string
    {
        return $this->operationId;
    }

    /**
     * @return array
     */
    public function getOperationArgs(): array
    {
        return $this->operationArgs;
    }

}