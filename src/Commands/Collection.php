<?php

namespace App\Commands;

use App\Interfaces\Command;

class Collection implements \Iterator
{
    private array $commands = [];
    private int $index = 0;

    public function clear(): void{
        $this->commands = [];
        $this->rewind();
    }

    public function add(Command $command): void
    {
        $this->commands[] = $command;
    }

    public function remove(int $index): void
    {
        unset($this->commands[$index]);
    }

    public function next(): void
    {
        ++$this->index;
    }

    public function current(): Command|null
    {
        return $this->commands[$this->index] ?? null;
    }

    public function key(): mixed
    {
        return $this->index;
    }

    public function valid(): bool
    {
        return !empty($this->commands) && $this->index <= array_key_last($this->commands);
    }

    public function rewind(): void
    {
        $this->commands = array_values($this->commands);
        $this->index = 0;
    }
}