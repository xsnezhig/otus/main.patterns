<?php

namespace App\Commands;

use App\Interfaces\Command;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JwtSignValidation implements Command
{
    public function __construct(
        private readonly string $pubKey,
        private readonly string $token,
        private readonly string $algorithm
    ) {

    }


    public function execute(): void
    {
        JWT::decode(
            $this->token,
            new Key($this->pubKey, $this->algorithm)
        );
    }
}