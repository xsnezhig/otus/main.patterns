<?php

namespace App\Commands;

use App\Exceptions\MoveExecutionException;
use App\Interfaces\Command;
use App\Interfaces\Movable;

class Move implements Command
{
    public function __construct(
        private Movable $movable
    ) {
    }

    /**
     * @throws MoveExecutionException
     */
    public function execute(): void
    {
        try {
            $this->movable->setPosition($this->movable->getPosition()->plus($this->movable->getVelocity()));
        } catch (\Throwable $e) {
            throw  new MoveExecutionException($e->getMessage());
        }
    }
}