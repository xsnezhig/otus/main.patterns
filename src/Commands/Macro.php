<?php

namespace App\Commands;

use App\Interfaces\Command;

class Macro implements Command
{
    public function __construct(
        private Collection $collection
    )
    {
    }

    public function execute(): void
    {
        while ($this->collection->valid()){
            $this->collection->current()?->execute();
            $this->collection->next();
        }
    }
}