<?php

namespace App\Commands;

use App\Interfaces\Command;
use App\Interfaces\Fuel;

class BurnFuel implements Command
{
    public function __construct(
        private Fuel $fuel
    ) {
    }

    public function execute(): void
    {
        $this->fuel->setAmount($this->fuel->getAmount() - $this->fuel->getConsumption());
    }
}