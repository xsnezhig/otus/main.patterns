<?php

namespace App\Commands;

use App\Container;
use App\ContainerInterface;
use App\Interfaces\Command;
use App\Interfaces\Queue;
use App\Interfaces\QueueAccess;
use App\Interfaces\QueueStateAccess;
use App\QueueState\MoveToState;

class MoveQueueCommand implements Command
{
    public function __construct(
        private readonly QueueStateAccess&QueueAccess $old,
        private readonly Queue                        $new,
        private readonly ContainerInterface           $container
    )
    {
    }

    public function execute(): void
    {
        $this->old->setState($this->container->make('queue:state.move:to', ['old' => $this->old, 'new' => $this->new]));
    }
}