<?php

namespace App\Commands;

use App\ContainerInterface;
use App\Interfaces\Command;
use App\Interfaces\Field;
use App\Interfaces\Position;

class CheckFieldPosition implements Command
{
    public function __construct(
        private readonly Position           $item,
        private readonly ContainerInterface $container
    )
    {
    }

    public function execute(): void
    {
        $commands = new Collection();
        /**@var Field $field*/
        $field = $this->container->make("ItemField:get", ['item' => $this->item]);
        if(!$this->isItemOnField($field)){
            $commands->add($this->container->make("ItemField:remove", ['item' => $this->item, 'field' => $field]));
            $field = $this->container->make("ItemField:findFor", ['item' => $this->item]);
            $commands->add($this->container->make("ItemField:add", ['item' => $this->item, 'field' => $field]));
        }
        foreach ($field->getItems() as $item){
            $commands->add($this->container->make("ItemField:check:collision", ['new' => $this->item, 'exist' => $item]));
        }

        $this->container->make("Command:macro", ['commands' => $commands])->execute();
    }

    private function isItemOnField(Field $field): bool
    {
        $from = $field->getFrom();
        $to = $field->getTo();
        $x = $this->item->getPosition()->getX();
        $y = $this->item->getPosition()->getY();
        return $from->getX() > $x
            && $from->getY() > $y
            && $to->getX() < $x
            && $to->getY() < $y;
    }
}