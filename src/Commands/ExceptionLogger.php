<?php

namespace App\Commands;

use App\Interfaces\Command;

class ExceptionLogger implements Command
{
    public function __construct(
        private string     $path,
        private \Throwable $throwable
    )
    {
    }

    public function execute(): void
    {
        file_put_contents($this->path, $this->throwable->getMessage(), FILE_APPEND);
    }
}