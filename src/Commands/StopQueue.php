<?php

namespace App\Commands;

use App\Exceptions\StopQueueException;
use App\Interfaces\Command;

class StopQueue implements Command
{

    /**
     * @return void
     * @throws StopQueueException
     */
    public function execute(): void
    {
        throw new StopQueueException();
    }
}