<?php

namespace App\Commands;

use App\Interfaces\Command;
use App\Interfaces\Rotatable;

class Rotate implements Command
{
    public function __construct(
        private Rotatable $rotatable
    ) {
    }

    public function execute(): void
    {
        $number = $this->rotatable->getDirectionNumber();
        $sum = ($this->rotatable->getVelocity() + $this->rotatable->getDirection());
        $this->rotatable->setDirection(($number + $sum) % $number);
    }
}