<?php

namespace App\Commands;

use App\Interfaces\AngularMove;
use App\Interfaces\Command;
use App\Vector;

class CorrectVelocity implements Command
{
    public function __construct(
        private AngularMove $move
    ) {
    }

    public function execute(): void
    {
        $v = abs(sqrt($this->move->getVelocity()->getX() ** 2 + $this->move->getVelocity()->getY() ** 2));
        $rads = deg2rad($this->move->getAngular() * (360 / $this->move->getAngularNumber()));
        $x = $v * cos($rads);
        $y = $v * sin($rads);

        $this->move->setVelocity(new Vector((int)$x, (int)$y));
    }
}