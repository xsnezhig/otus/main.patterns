<?php

namespace App\Commands;

use App\Interfaces\Command;
use App\Interfaces\QueueStateAccess;

class StopQueueState implements Command
{
    public function __construct(
        private readonly QueueStateAccess $queue
    )
    {

    }

    public function execute(): void
    {
        $this->queue->setState(null);
    }
}