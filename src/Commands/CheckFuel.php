<?php

namespace App\Commands;

use App\Interfaces\Command;
use App\Interfaces\Fuel;
use App\Interfaces\FuelInfo;
use Exception;

class CheckFuel implements Command
{
    public function __construct(
        private FuelInfo $fuel
    ) {
    }

    /**
     * @return void
     * @throws Exception
     */
    public function execute(): void
    {
        if ($this->fuel->getConsumption() > $this->fuel->getAmount()) {
            throw new \RuntimeException('Amount of fuel is not enough');
        }
    }
}