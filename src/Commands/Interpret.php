<?php

namespace App\Commands;

use App\ContainerInterface;
use App\Interfaces\Command;
use App\Interfaces\Queue;

class Interpret implements Command
{
    public function __construct(
        private readonly int                $gameId,
        private readonly int                $objectId,
        private readonly string             $operationId,
        private readonly array              $operationArgs,
        private readonly ContainerInterface $container,
        private readonly Queue              $queue
    ) {
    }

    public function execute(): void
    {
        //Получаем класс команды по её ID
        $commandClass = $this->container->make('Interpret:resolve.command', ['id' => $this->operationId]);
        //Получаем объект по его ID и ID игры
        $obj = $this->container->make('Interpret:resolve:obj', ['id' => $this->objectId, 'gameId' => $this->gameId]);
        //Получаем команду по её классу, передавая ей объект и аргументы
        $command = $this->container->make($commandClass, ['obj' => $obj, 'args' => $this->operationArgs]);
        //Добавляем команду в очередь
        $this->queue->add($command);
    }
}