<?php

namespace App\Commands\Repeaters;

use App\Interfaces\Command;

class Repeater implements Command
{
    public function __construct(
        private Command $command
    )
    {
    }

    public function execute(): void
    {
        $this->command->execute();
    }
}