<?php

namespace App;

class StaticVector
{
    public function __construct(
        protected int $x,
        protected int $y
    ) {
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }
}