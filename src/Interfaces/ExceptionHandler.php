<?php

namespace App\Interfaces;

interface ExceptionHandler
{
    public function handle(Command $command, \Throwable $throwable): void;
}