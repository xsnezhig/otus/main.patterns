<?php

namespace App\Interfaces;

use App\Vector;

interface AngularMove
{
    public function getAngular(): int;
    public function getAngularNumber(): int;
    public function getVelocity(): Vector;
    public function setVelocity(Vector $vector): void;
}