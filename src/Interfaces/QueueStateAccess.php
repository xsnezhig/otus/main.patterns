<?php

namespace App\Interfaces;

use App\Commands\Collection;

interface QueueStateAccess
{
    public function setState(?QueueState $state): void;
}