<?php

namespace App\Interfaces;

use App\Vector;

interface Position
{
    public function getPosition(): Vector;
}