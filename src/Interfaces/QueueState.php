<?php

namespace App\Interfaces;

interface QueueState
{
    public function execute(Command $command): void;
}