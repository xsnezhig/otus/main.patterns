<?php

namespace App\Interfaces;

interface Rotatable
{
    public function getVelocity(): int;

    public function getDirection(): int;

    public function setDirection(int $direction): void;

    public function getDirectionNumber(): int;
}