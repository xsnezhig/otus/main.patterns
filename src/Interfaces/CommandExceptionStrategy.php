<?php

namespace App\Interfaces;

interface CommandExceptionStrategy
{
    public function create(Command $command, \Throwable $throwable): Command;
    public function supportCommand(): string;
    public function supportException(): string|null;
}