<?php

namespace App\Interfaces;

use App\StaticVector;

interface Field
{
    /**
     * @return array<Position>
     */
    public function getItems(): array;

    public function addItem(Position $item): void;

    public function deleteItem(Position $item): void;

    public function getFrom(): StaticVector;
    
    public function getTo(): StaticVector;
}