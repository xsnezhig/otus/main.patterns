<?php

namespace App\Interfaces;

interface Queue
{
    public function run(): void;

    public function add(Command $command): void;

}