<?php

namespace App\Interfaces;

interface FuelInfo
{
    public function getAmount(): int;

    public function getConsumption(): int;
}