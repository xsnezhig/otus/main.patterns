<?php

namespace App\Interfaces;

interface Fuel
{
    public function getAmount(): int;

    public function setAmount(int $amount): void;

    public function getConsumption(): int;
}