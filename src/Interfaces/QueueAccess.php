<?php

namespace App\Interfaces;

use App\Commands\Collection;

interface QueueAccess
{
    public function getCollection(): Collection;
}