<?php

namespace App\Interfaces;

use App\Vector;

interface Movable
{
    public function getPosition(): Vector;
    public function getVelocity(): Vector;
    public function setPosition(Vector $vector): void;
}