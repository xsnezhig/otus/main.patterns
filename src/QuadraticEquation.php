<?php
declare(strict_types=1);

namespace App;

use RuntimeException;

class QuadraticEquation
{
    /**@return array<int>
     * @throws RuntimeException
     */
    public function solve(float $a, float $b = 0, float $c = 0, float $eps = null): array
    {
        if (is_infinite($a) || is_nan($a)) {
            throw new RuntimeException("A values can not be NaN or INF");
        }

        $eps = $eps ?? (float)(10 ** -5);

        if (($a >= 0.0 && $eps > $a) || ($a <= 0.0 && -$eps > $a)) {
            throw new RuntimeException("A value must be out of range $eps - " . -$eps . ". (got $a)");
        }

        $dis = ($b ** 2) - (4 * $a * $c);

        if ($dis < 0) {
            return [];
        }

        $roots = [];
        if ($dis > $eps) {
            $roots[] = (-$b + sqrt($dis)) / (2 * $a);
            $roots[] = (-$b - sqrt($dis)) / (2 * $a);
        } else {
            $roots[] = -$b / (2 * $a);
        }

        return
            array_map(
                static fn(float $v) => floor($v) === $v ? (int)$v : $v,
                $roots
            );
    }
}