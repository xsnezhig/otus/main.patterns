<?php

namespace App;


use App\Commands\JwtSignValidation;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

class Router
{
    public function run(): void
    {
        $dispatcher = simpleDispatcher(fn($r) => $this->configure($r));
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);

        $info = $dispatcher->dispatch($httpMethod, $uri);
        switch ($info[0]) {
            case Dispatcher::FOUND:
            {
                $info[1]($info[2]);
                break;
            }
        }
    }

    private function configure(RouteCollector $r): void
    {
        $r->post('/check', fn() => Container::instance()->make(
            JwtSignValidation::class,
            [
                'pubKey'    => file_get_contents(getenv('ROOT') . getenv('AUTH_PUB')),
                'algorithm' => getenv('ALGORITHM'),
                'token'       => str_replace('Bearer ', '', $_SERVER['HTTP_AUTHORIZATION'])
            ]
        )->execute());
    }
}



