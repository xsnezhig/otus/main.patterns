<?php

namespace App\QueueState;

use App\Interfaces\Command;
use App\Interfaces\Queue;
use App\Interfaces\QueueAccess;
use App\Interfaces\QueueState;

class MoveToState implements QueueState
{
    public function __construct(
        private readonly QueueAccess $old,
        private readonly Queue       $new
    )
    {
    }

    public function execute(Command $command): void
    {
        $this->new->add($command);
        while ($this->old->getCollection()->valid()) {
            $c = $this->old->getCollection()->current();
            if ($c && $c !== $command) {
                $this->new->add($c);
            }
            $this->old->getCollection()->next();
        }
        $this->old->getCollection()->clear();
    }
}