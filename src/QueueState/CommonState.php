<?php

namespace App\QueueState;

use App\Interfaces\Command;
use App\Interfaces\QueueState;

class CommonState implements QueueState
{
    public function execute(Command $command): void
    {
        $command->execute();
    }
}