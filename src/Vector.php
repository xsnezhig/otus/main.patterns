<?php

namespace App;

class Vector extends StaticVector
{
    public function plus(self $vector): self
    {
        $this->x += $vector->x;
        $this->y += $vector->y;
        return $this;
    }
}