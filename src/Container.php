<?php

namespace App;


use App\Exceptions\ArgumentNotResolved;
use App\Exceptions\ClassNotInstantiable;
use App\Exceptions\NotSupportedTypeException;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionUnionType;

class Container implements ContainerInterface, ContainerRegisterInterface
{

    private array $instances = [];

    private array $callbacks = [];

    private array $rules = [];

    private static Container|null $container = null;

    private function __construct()
    {
    }

    public static function instance(): self
    {
        if (is_null(self::$container)) {
            self::$container = new self;
        }
        return self::$container;
    }

    /**
     * @throws \ReflectionException
     */
    private function resolve(string $id, callable|array $options = []): mixed
    {
        if (($this->callbacks[$id] ?? null) instanceof ContainerCallbackInterface) {
            return $this->callbacks[$id]->execute($this, $options);
        }

        $reflection = new \ReflectionClass($id);
        if (!$reflection->isInstantiable()) {
            throw new ClassNotInstantiable(sprintf(
                'Class %s is not instantiable',
                $reflection->getName()
            ));
        }

        $constructor = $reflection->getConstructor();
        if ($constructor === null) {
            return $reflection->newInstance();
        }
        $args = $this->resolveArguments($id, $constructor, $options);
        return $reflection->newInstanceArgs($args);
    }

    private function resolveArguments(string $id, \ReflectionMethod $method, array $arguments): array
    {
        $args = [];
        $params = $method->getParameters();
        foreach ($params as $param) {
            if (array_key_exists($param->getName(), $arguments)) {
                $args[] = $arguments[$param->getName()];
            } elseif (isset($this->rules[$id][$param->getName()])) {
                $args[] = $this->rules[$id][$param->getName()];
            } elseif ($param->isDefaultValueAvailable()) {
                $args[] = $param->getDefaultValue();
            } elseif ($param->getType() !== null) {
                $args[] = $this->resolveType($param->getType());
            }
        }
        return $args;
    }

    private function resolveType(ReflectionNamedType|ReflectionUnionType|ReflectionIntersectionType $type)
    {
        if ($type instanceof ReflectionNamedType === false) {
            throw new NotSupportedTypeException("Union and Intersection types are not supported");
        }
        if (!class_exists($type->getName())) {
            throw new ArgumentNotResolved("Argument with type $type is not resolved");
        }

        return $this->resolve($type->getName());

    }

    public function get(string $id)
    {
        if (!array_key_exists($id, $this->instances)) {
            $this->instances[$id] = $this->resolve($id);
        }
        return $this->instances[$id];
    }

    public function has(string $id): bool
    {
        return array_key_exists($id, $this->callbacks)
            || array_key_exists($id, $this->instances)
            || array_key_exists($id, $this->rules);
    }

    public function make($id, array $options = []): mixed
    {
        return $this->resolve($id, $options);
    }

    public function register(string $id, ContainerCallbackInterface|array $options = []): void
    {
        if ($options instanceof ContainerCallbackInterface) {
            $this->callbacks[$id] = $options;
        } else {
            $this->rules[$id] = $options;
        }
    }
}