<?php

namespace App\Sort;

interface SortInterface
{
    public function sort(array $data): array;
}