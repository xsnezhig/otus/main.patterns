<?php

namespace App\Sort;

class Selection implements SortInterface
{

    public function sort(array $data): array
    {
        $size = count($data);

        for ($i = 0; $i < $size - 1; $i++) {
            $min = $i;

            for ($j = $i + 1; $j < $size; $j++) {
                if ($data[$j] < $data[$min]) {
                    $min = $j;
                }
            }

            $temp = $data[$i];
            $data[$i] = $data[$min];
            $data[$min] = $temp;
        }
        return $data;
    }
}