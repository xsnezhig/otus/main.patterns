<?php

namespace App\Sort;

class Merge implements SortInterface
{

    public function sort(array $data): array
    {
        $count = count($data);
        if ($count <= 1) {
            return $data;
        }

        $left = array_slice($data, 0, (int)($count / 2));
        $right = array_slice($data, (int)($count / 2));

        $left = $this->sort($left);
        $right = $this->sort($right);

        return $this->merge($left, $right);
    }

    public function merge(array $left, array $right): array
    {
        $ret = array();
        while (count($left) > 0 && count($right) > 0) {
            if ($left[0] < $right[0]) {
                $ret[] = array_shift($left);
            } else {
                $ret[] = array_shift($right);
            }
        }

        array_splice($ret, count($ret), 0, $left);
        array_splice($ret, count($ret), 0, $right);

        return $ret;
    }
}