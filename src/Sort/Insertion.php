<?php

namespace App\Sort;

class Insertion implements SortInterface
{

    public function sort(array $data): array
    {
        $count = count($data);
        if ($count <= 1) {
            return $data;
        }

        for ($i = 1; $i < $count; $i++) {
            $cur_val = $data[$i];
            $j = $i - 1;

            while (isset($data[$j]) && $data[$j] > $cur_val) {
                $data[$j + 1] = $data[$j];
                $data[$j] = $cur_val;
                $j--;
            }
        }

        return $data;
    }
}