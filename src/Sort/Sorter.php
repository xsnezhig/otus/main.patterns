<?php

namespace App\Sort;

use App\Container;
use JetBrains\PhpStorm\ExpectedValues;

class Sorter
{
    public function __construct(
        private readonly string $in,
        private readonly string $out,
        #[ExpectedValues(['merge', 'selection', 'insertion'])]
        private readonly string $method,
    )
    {
    }

    public function execute(): void
    {
        /**@var SortInterface $sort */
        $sort = Container::instance()->get(__NAMESPACE__ . '\\' . ucfirst($this->method));
        file_put_contents(
            $this->out,
            implode(
                ',',
                $sort->sort(
                    explode(
                        ',',
                        file_get_contents($this->in))
                )
            )
        );
    }
}