<?php

namespace Tests\Interfaces;

interface InterfaceForAdapter
{
    public function getSpecific(): string;

    public function setSpecific(string $value): void;

    public function doSomethingComplicated(object $obj, InterfaceForAdapter $adapter): InterfaceForAdapter;
}