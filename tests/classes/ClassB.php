<?php

namespace Tests\Classes;

class ClassB
{
    public function __construct(
        private ClassA      $classA,
        private string|null $extend = null
    )
    {
    }

    public function getParams(): array
    {
        return [
            'classA' => $this->classA,
            'extend' => $this->extend
        ];
    }
}