<?php

namespace Tests\Unit\QueueState;

use App\Commands\Collection;
use App\Interfaces\Command;
use App\Interfaces\Queue;
use App\Interfaces\QueueAccess;
use App\QueueState\MoveToState;
use PHPUnit\Framework\TestCase;

class MoveToStateTest extends TestCase
{
    public function testExecute(): void
    {
        $firstCommand = $this->createMock(Command::class);
        $secondCommand = $this->createMock(Command::class);
        $collection = new Collection();
        $collection->add($firstCommand);
        $collection->add($secondCommand);

        $old = $this->createMock(QueueAccess::class);
        $old->method('getCollection')->willReturn($collection);

        $new = $this->createMock(Queue::class);
        $new->expects($this->exactly(2))->method('add')->withConsecutive([$firstCommand], [$secondCommand]);

        (new MoveToState($old, $new))->execute($firstCommand);

        $this->assertEquals(0, $collection->key());
        $this->assertFalse($collection->valid());
    }
}