<?php

namespace Tests\Unit\Commands;

use App\Commands\StopQueueState;
use App\Interfaces\QueueStateAccess;
use PHPUnit\Framework\TestCase;

class StopQueueStateTest extends TestCase
{
    public function testExecute(): void{
        $queue = $this->createMock(QueueStateAccess::class);
        $queue->expects($this->once())->method('setState')->with(null);

        (new StopQueueState($queue))->execute();
    }
}