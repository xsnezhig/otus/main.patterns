<?php

namespace Tests\Unit\Commands;

use App\Commands\CheckFuel;
use App\Interfaces\Fuel;
use App\Interfaces\FuelInfo;
use PHPUnit\Framework\TestCase;

class CheckFuelTest extends TestCase
{
    private function executeProvider(): array
    {
        return [
            [10, 0, false],
            [10, 10, false],
            [0, 10, true],
            [2, 4, true],
            [0, 0, false]
        ];
    }

    /**
     * @dataProvider executeProvider
     */

    public function testExecution(int $amount, int $consumption, bool $expectException): void
    {
        $mock = $this->createConfiguredMock(FuelInfo::class, [
            'getAmount'      => $amount,
            'getConsumption' => $consumption
        ]);

        if ($expectException) {
            $this->expectExceptionMessage('Amount of fuel is not enough');
        }

        (new CheckFuel($mock))->execute();

        $this->addToAssertionCount(1);
    }
}