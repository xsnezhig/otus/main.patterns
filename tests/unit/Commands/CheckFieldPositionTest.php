<?php

namespace Tests\Unit\Commands;

use App\Commands\CheckFieldPosition;
use App\Commands\Collection;
use App\ContainerInterface;
use App\Interfaces\Command;
use App\Interfaces\Field;
use App\Interfaces\Position;
use App\StaticVector;
use App\Vector;
use PHPUnit\Framework\TestCase;

class CheckFieldPositionTest extends TestCase
{
    public function testExecute(): void
    {
        $commands = [
            'remove' => $this->createMock(Command::class),
            'add' => $this->createMock(Command::class),
            'checkFoo' => $this->createMock(Command::class),
            'checkBar' => $this->createMock(Command::class),
        ];
        $item = $this->createMock(Position::class);
        $item->method('getPosition')->willReturn(new Vector(2, 6));
        $container = $this->createMock(ContainerInterface::class);
        $field = $this->createMock(Field::class);
        $field->method('getFrom')->willReturn(new StaticVector(1, 2));
        $field->method('getTo')->willReturn(new StaticVector(5, 7));

        $existFoo = $this->createMock(Position::class);
        $existBar = $this->createMock(Position::class);
        $newField = $this->createMock(Field::class);
        $newField->method('getItems')->willReturn([$existBar, $existFoo]);

        $macro = $this->createMock(Command::class);
        $macro->expects($this->once())->method('execute');

        $collection = new Collection();
        foreach ($commands as $command) {
            $collection->add($command);
        }

        $container->method('make')->withConsecutive(
            ['ItemField:get', ['item' => $item]],
            ["ItemField:remove", ['item' => $item, 'field' => $field]],
            ["ItemField:findFor", ['item' => $item]],
            ["ItemField:add", ['item' => $item, 'field' => $newField]],
            ["ItemField:check:collision", ['new' => $item, 'exist' => $existFoo]],
            ["ItemField:check:collision", ['new' => $item, 'exist' => $existBar]],
            ['Command:macro', ['commands' => $collection]]
        )->willReturnOnConsecutiveCalls(
            $field,
            $commands['remove'],
            $newField,
            $commands['add'],
            $commands['checkFoo'],
            $commands['checkBar'],
            $macro
        );

        (new CheckFieldPosition($item, $container))->execute();
    }
}