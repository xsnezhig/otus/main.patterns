<?php

namespace Tests\Unit\Commands;

use App\Commands\JwtSignValidation;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use PHPUnit\Framework\TestCase;

class JwtSignValidationTest extends TestCase
{
    private function generatePair(): array
    {
        $res = openssl_pkey_new(['private_key_type' => OPENSSL_KEYTYPE_RSA, 'private_key_bits' => 4096]);
        openssl_pkey_export($res, $private);
        $public = openssl_pkey_get_details($res)['key'];
        return [$private, $public];
    }

    public function testExecuteSuccess(): void
    {
        [$private, $public] = $this->generatePair();
        $jwt = JWT::encode(['id' => random_int(1, 1000)], $private, 'RS256');

        (new JwtSignValidation($public, $jwt, 'RS256'))->execute();
        $this->addToAssertionCount(1);
    }

    public function testExecuteWrong(): void
    {
        $this->expectException(SignatureInvalidException::class);
        [$private] = $this->generatePair();
        $jwt = JWT::encode([], $private, 'RS256');
        [, $public] = $this->generatePair();
        (new JwtSignValidation($public, $jwt, 'RS256'))->execute();
    }
}