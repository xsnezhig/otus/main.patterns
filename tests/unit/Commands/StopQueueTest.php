<?php

namespace Tests\Unit\Commands;

use App\Commands\StopQueue;
use App\Exceptions\StopQueueException;
use PHPUnit\Framework\TestCase;

class StopQueueTest extends TestCase
{
    public function testExecute(): void{
        $this->expectException(StopQueueException::class);
        (new StopQueue())->execute();
    }
}