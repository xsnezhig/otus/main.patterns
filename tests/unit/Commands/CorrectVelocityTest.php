<?php

namespace Tests\Unit\Commands;

use App\Commands\CorrectVelocity;
use App\Interfaces\AngularMove;
use App\Vector;
use PHPUnit\Framework\TestCase;

class CorrectVelocityTest extends TestCase
{
    private function executeProvider(): array
    {
        return [
            [0, 8, new Vector(0, 1), new Vector(1, 0)],
            [2, 8, new Vector(-2, 1), new Vector(0, 2)]
        ];
    }

    /**
     * @dataProvider executeProvider
     */
    public function testExecute(int $angular, int $number, Vector $velocity, Vector $expected): void
    {
        $mock = $this->createConfiguredMock(AngularMove::class, [
            'getVelocity'      => $velocity,
            'getAngular'       => $angular,
            'getAngularNumber' => $number
        ]);

        $mock->expects(self::once())
             ->method('setVelocity')
             ->with($expected);

        (new CorrectVelocity($mock))->execute();
        $this->addToAssertionCount(1);
    }
}