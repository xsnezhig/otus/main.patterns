<?php

namespace Tests\Unit\Commands;

use App\Commands\Rotate;
use App\Interfaces\Rotatable;
use PHPUnit\Framework\TestCase;

class RotateTest extends TestCase
{
    private function setDirectionPositiveProvider(): array
    {
        //d - direction
        //n - directionNumber
        //v - velocity
        //e - expected
        return [
            ['d' => 2, 'v' => 2, 'n' => 8, 'e' => 4],
            ['d' => 0, 'v' => 10, 'n' => 8, 'e' => 2],
            ['d' => 0, 'v' => -1, 'n' => 8, 'e' => 7],
        ];
    }

    /**
     * @dataProvider  setDirectionPositiveProvider
     */
    public function testSetDirectionPositive(int $direction, int $velocity, int $number, int $expected): void
    {
        $mock = $this->createConfiguredMock(Rotatable::class, [
            'getVelocity'        => $velocity,
            'getDirection'       => $direction,
            'getDirectionNumber' => $number
        ]);

        $mock->expects($this->once())
             ->method('setDirection')
             ->with($this->equalTo($expected));

        (new Rotate($mock))->execute();
    }
}