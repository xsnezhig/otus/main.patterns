<?php

namespace Tests\Unit\Commands;

use App\Commands\MoveQueueCommand;
use App\ContainerInterface;
use App\Interfaces\Queue;
use App\Interfaces\QueueAccess;
use App\Interfaces\QueueStateAccess;
use App\QueueState\MoveToState;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;

class MoveQueueCommandTest extends TestCase
{
    private Prophet $prophet;

    public function testExecute()
    {
        $state = $this->createMock(MoveToState::class);
        $old = $this->prophet->prophesize()
            ->willImplement(QueueStateAccess::class)
            ->willImplement(QueueAccess::class);
        $old->setState($state)->shouldBeCalledTimes(1);

        $new = $this->createMock(Queue::class);

        $container = $this->createMock(ContainerInterface::class);
        $container->expects($this->once())
            ->method('make')
            ->with('queue:state.move:to', ['old' => $old->reveal(), 'new' => $new])
            ->willReturn($state);

        (new MoveQueueCommand($old->reveal(), $new, $container))->execute();
    }

    protected function setUp(): void
    {
        $this->prophet = new Prophet();
    }

    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
    }
}