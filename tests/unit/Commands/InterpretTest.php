<?php

namespace Tests\Unit\Commands;

use App\Commands\Interpret;
use App\Container;
use App\Interfaces\Command;
use App\Interfaces\Queue;
use Exception;
use PHPUnit\Framework\TestCase;
use Tests\Classes\ClassA;

class InterpretTest extends TestCase
{

    /**
     * @throws Exception
     */
    public function testExecute(): void
    {
        $gameId = random_int(1, 100);
        $objectId = random_int(101, 200);
        $operationId = random_bytes(10);
        $operationArgs = ['foo' => random_int(100, 1000), 'bar' => random_bytes(100)];
        $mockContainer = $this->createMock(Container::class);
        $mockQueue = $this->createMock(Queue::class);
        $mockCommand = $this->createMock(Command::class);
        $mockObject = $this->createMock(ClassA::class);
        $command = new Interpret(
            $gameId,
            $objectId,
            $operationId,
            $operationArgs,
            $mockContainer,
            $mockQueue
        );


        $mockQueue->expects($this->once())->method('add')->with($mockCommand);
        $mockContainer
            ->expects($this->exactly(3))->method('make')
            ->withConsecutive(
                ['Interpret:resolve.command', ['id' => $operationId]],
                ['Interpret:resolve:obj', ['id' => $objectId, 'gameId' => $gameId,]],
                [$mockCommand::class, ['obj' => $mockObject, 'args' => $operationArgs]]
            )
            ->willReturnOnConsecutiveCalls($mockCommand::class, $mockObject, $mockCommand);

        $command->execute();

    }
}