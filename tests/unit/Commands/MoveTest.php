<?php

namespace Tests\Unit\Commands;

use App\Commands\Move;
use App\Exceptions\MoveExecutionException;
use App\Interfaces\Movable;
use App\Vector;
use Exception;
use PHPUnit\Framework\TestCase;

class MoveTest extends TestCase
{
    private function setPositionPositiveProvider(): array
    {
        $raw = [
            ['p' => [12, 5], 'v' => [-7, 3], 'e' => [5, 8]],
            ['p' => [-10, 4], 'v' => [-5, -10], 'e' => [-15, -6]],
        ];
        return array_map(
            static fn($item) => [
                new Vector(...$item['p']),
                new Vector(...$item['v']),
                new Vector(...$item['e'])
            ],
            $raw
        );
    }

    /**
     * @dataProvider  setPositionPositiveProvider
     * @throws MoveExecutionException
     */
    public function testSetPositionPositive(Vector $velocity, Vector $position, Vector $expected): void
    {
        $mock = $this->createMock(Movable::class);
        $mock->method('getPosition')->willReturn($position);
        $mock->method('getVelocity')->willReturn($velocity);
        $mock->expects($this->once())
             ->method('setPosition')
             ->with($this->equalTo($expected));

        (new Move($mock))->execute();
    }


    private function exceptionsProvider(): array
    {
        $vectorStub = $this->createStub(Vector::class);
        $vectorStub->method('plus')->willReturnSelf();

        $noVelocity = $this->createConfiguredMock(Movable::class, ['getPosition' => $vectorStub]);
        $noVelocity->method('getVelocity')
                   ->willThrowException(new Exception('Velocity is not exists'));

        $noPosition = $this->createConfiguredMock(Movable::class, ['getVelocity' => $vectorStub]);
        $noPosition->method('getPosition')
                   ->willThrowException(new Exception('Position is not exists'));

        $positionIsNotChangeable = $this->createConfiguredMock(Movable::class, [
            'getVelocity' => $vectorStub,
            'getPosition' => $vectorStub,
        ]);
        $positionIsNotChangeable->method('setPosition')
                                ->willThrowException(new Exception('Could not change position'));

        return [
            [$noVelocity, 'Velocity is not exist'],
            [$noPosition, 'Position is not exist'],
            [$positionIsNotChangeable, 'Could not change position'],
        ];

    }

    /**
     * @dataProvider  exceptionsProvider
     * @throws MoveExecutionException
     */
    public function testExceptions(Movable $movable, string $error): void
    {
        $this->expectExceptionObject(new MoveExecutionException($error));
        (new Move($movable))->execute();
    }
}