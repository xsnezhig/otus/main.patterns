<?php

namespace Tests\Unit\Commands;

use App\Commands\BurnFuel;
use App\Interfaces\Fuel;

class BurnFuelTest extends \PHPUnit\Framework\TestCase
{
    private function executeProvider(): array
    {
        return [
            [10, 10, 0],
            [5, 7, -2],
            [0, 0, 0]
        ];
    }

    /**
     * @dataProvider executeProvider
     */
    public function testExecute(int $amount, int $consumption, int $expected): void
    {
        $mock = $this->createConfiguredMock(Fuel::class, [
            'getAmount' => $amount,
            'getConsumption' => $consumption
        ]);

        $mock->expects($this->once())->method('getAmount');
        $mock->expects($this->once())->method('getAmount');
        $mock->expects($this->once())->method('setAmount')->with($expected);

        (new BurnFuel($mock))->execute();
    }
}