<?php

namespace Tests\Unit\Commands;

use App\Commands\BurnFuel;
use App\Commands\CheckFuel;
use App\Commands\Collection;
use App\Commands\Macro;
use App\Commands\Move;
use PHPUnit\Framework\TestCase;

class MacroTest extends TestCase
{
    public function testCheckMoveBurn(): void
    {
        $check = $this->createMock(CheckFuel::class);
        $move = $this->createMock(Move::class);
        $burn = $this->createMock(BurnFuel::class);

        $collection = new Collection();
        $executed = 0;

        //Assert that method will be executed once in the right order
        foreach ([$check, $move, $burn] as $index => $item) {
            $collection->add($item);
            $item->expects($this->once())
                 ->method('execute')
                 ->willReturnCallback(function () use ($index, &$executed) {
                     $this->assertEquals($index, $executed);
                     ++$executed;
                 });
        }

        (new Macro($collection))->execute();

    }
}