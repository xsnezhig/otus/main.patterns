<?php

namespace Tests\Unit;

use App\Commands\Repeaters\Repeater;
use App\Commands\Repeaters\SecondRepeater;
use App\ExceptionHandler;
use App\Interfaces\Command;
use App\Interfaces\CommandExceptionStrategy;
use App\Interfaces\Queue;
use PHPUnit\Framework\TestCase;

class ExceptionHandlerTest extends TestCase
{

    public function testHandleWithoutSpecialException(): void
    {
        $command = $this->createMock(Command::class);
        $strategy = $this->createConfiguredMock(CommandExceptionStrategy::class, [
            'supportCommand' => $command::class
        ]);

        $strategy->expects($this->once())->method('create')->with($command, $this->isInstanceOf(\Throwable::class));

        $exception = $this->getMockBuilder(\Throwable::class)
                          ->disableOriginalConstructor()->setMockClassName('Special_Exception')->getMock();
        $strategyWithException = $this->createConfiguredMock(CommandExceptionStrategy::class, [
            'supportCommand'   => $command::class,
            'supportException' => $exception::class
        ]);

        $strategyWithException->expects($this->once())->method('create')->with($command, $exception);

        $handler = new ExceptionHandler(
            $this->createMock(Queue::class),
            [$strategy, $strategyWithException]
        );

        $handler->handle($command, $this->createMock(\Throwable::class));
        $handler->handle($command, $exception);

    }

    public function testHandleStrategyChain(): void
    {
        //Создаём команду, которая бросила исключение и повторители
        $command = $this->createMock(Command::class);
        $repeater = new Repeater($command);
        $secondRepeater = new SecondRepeater($repeater);
        $argumentException = $this->createMock(\InvalidArgumentException::class);

        //Создаём стратегию на первую команду
        $strategy = $this->createConfiguredMock(CommandExceptionStrategy::class, [
            'supportCommand'   => $command::class,
            'supportException' => $argumentException::class
        ]);
        //Проверяем, что она вызвана с нужной командой и ошибкой
        $strategy->expects($this->once())
                 ->method('create')
                 ->with($command, $argumentException)
                 ->willReturn($repeater);

        //Создаём стратегию для повторения
        $repeatStrategy = $this->createConfiguredMock(CommandExceptionStrategy::class, [
            'supportCommand'   => Repeater::class,
            'supportException' => $argumentException::class
        ]);

        $repeatStrategy->expects($this->once())
                       ->method('create')
                       ->with($repeater, $argumentException)
                       ->willReturn($secondRepeater);

        //Создаём финальную стратегию и команду, которая может, например, писать в лог.
        $finalCommand = $this->createMock(Command::class);
        $finalStrategy = $this->createConfiguredMock(CommandExceptionStrategy::class, [
            'supportCommand'   => SecondRepeater::class,
            'supportException' => $argumentException::class
        ]);
        $finalStrategy->expects($this->once())
                      ->method('create')
                      ->with($secondRepeater, $argumentException)
                      ->willReturn($finalCommand);

        //Создаём очередь и проверяем, что метод add был вызван в нужными параметрами в нужном порядке
        $queue = $this->createMock(Queue::class);
        $queue->expects($this->exactly(3))
              ->method('add')
              ->withConsecutive(
                  [$repeater],
                  [$secondRepeater],
                  [$finalCommand]
              );

        $handler = new ExceptionHandler($queue, [$strategy, $repeatStrategy, $finalStrategy]);

        //Имитируем выброс исключений.
        $handler->handle($command, $argumentException);
        $handler->handle($repeater, $argumentException);
        $handler->handle($secondRepeater, $argumentException);
    }
}