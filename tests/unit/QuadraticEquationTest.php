<?php

namespace Tests\Unit;

use App\QuadraticEquation;
use JetBrains\PhpStorm\ArrayShape;
use PHPUnit\Framework\TestCase;

class QuadraticEquationTest extends TestCase
{
    private QuadraticEquation $equation;

    protected function setUp(): void
    {
        $this->equation = new QuadraticEquation();
    }


    private function solvePositiveExpectationProvider(): array
    {
        return [
            [["a" => 1, "c" => 1], []], //x^2+1=0; no
            [["a" => 1, "c" => -1], [1, -1]], //x^2-1=0; x1 = 1, x2 = 2
            [["a" => 1, "b" => 2, "c" => 1], [-1]], //x^2+2+1; x = -1
            [["a" => 1, "b" => 10 ** -3], [-0.0005]] //x^2-0.001x; x = -0.0005
        ];
    }

    /**
     * @dataProvider solvePositiveExpectationProvider
     */
    public function testSolvePositiveExpectation(
        #[ArrayShape(["a" => "float", "b" => "float", "c" => "float"])]
        array $numbers,
        array $expected
    ): void {
        $result = $this->equation->solve(...$numbers);
        $this->assertEquals($expected, $result);
    }

    private function solverTrowExceptionWithZeroAProvider(): array
    {
        return [[0], [0.0]];
    }

    /**
     * @dataProvider solverTrowExceptionWithZeroAProvider
     */
    public function testSolverThrowExceptionWithZeroA($a): void
    {
        $this->expectException(\Exception::class);
        $this->equation->solve($a);
    }

    private function noNumberValuesProvider(): array
    {
        return [[NAN], [INF]];
    }

    /**
     * @dataProvider noNumberValuesProvider
    */
    public function testNoNumericValues($a): void
    {
        $this->expectException(\Exception::class);
        $this->equation->solve($a);
    }
}