<?php

namespace Tests\Unit;

use App\Container;
use PHPUnit\Framework\TestCase;
use Tests\Classes\ClassA;
use Tests\Classes\ClassB;

class ContainerTest extends TestCase
{
    public function testGetClass(): void
    {
        $container = Container::instance();
        $container->register(ClassB::class);
        self::assertTrue($container->has(ClassB::class));
        $resolved = $container->get(ClassB::class);
        $this->assertIsObject($resolved);
        self::assertInstanceOf(ClassB::class, $resolved);
        self::assertSame($resolved, $container->get(ClassB::class));
    }

    public function testMake(): void
    {
        $container = Container::instance();
        $resolved = $container->make(ClassB::class);
        self::assertNotSame($resolved, $container->make(ClassB::class));
    }

    public function testMakeWithOptions(): void
    {
        $options = [
            'extend' => random_bytes(100),
            'classA' => $this->createMock(ClassA::class)
        ];
        $container = Container::instance();
        $container->register(ClassB::class, $options);

        /**@var ClassB $resolved */
        $resolved = $container->make(ClassB::class);
        self::assertInstanceOf(ClassB::class, $resolved);
        self::assertEquals($options, $resolved->getParams());
    }

}