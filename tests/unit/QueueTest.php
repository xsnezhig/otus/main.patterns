<?php

namespace Tests\Unit;

use App\Interfaces\Command;
use App\Interfaces\ExceptionHandler;
use App\Queue;
use PHPUnit\Framework\TestCase;

class QueueTest extends TestCase
{
    public function testRun(): void
    {
        $handler = $this->createMock(ExceptionHandler::class);
        $queue = new Queue($handler);
        $commands = [$this->createMock(Command::class)];

        $broken = $this->createMock(Command::class);
        $exception = $this->createMock(\Throwable::class);
        $broken->method('execute')->willThrowException($exception);

        $commands[] = $broken;
        $commands[] = $this->createMock(Command::class);

        foreach ($commands as $command) {
            $queue->add($command);
            $command->expects($this->once())->method('execute');
        }

        $handler->expects($this->once())->method('handle')->with($broken, $exception);

        $queue->run();

    }
}