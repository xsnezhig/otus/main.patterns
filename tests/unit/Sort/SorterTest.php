<?php

namespace Tests\Unit\Sort;

use App\Container;
use App\ContainerCallbackInterface;
use App\Sort\Insertion;
use App\Sort\Merge;
use App\Sort\Selection;
use App\Sort\Sorter;
use PHPUnit\Framework\TestCase;

class SorterTest extends TestCase
{
    private string $in = __DIR__ . '/../../files/sort_in.txt';
    private string $out = __DIR__ . '/../../files/sort_out.txt';

    protected function tearDown(): void
    {
        unlink($this->out);
    }


    private function executeProvider(): array
    {

        return [
            ['selection', Selection::class],
            ['merge', Merge::class],
            ['insertion', Insertion::class]
        ];
    }


    /**
     * @param string $method
     * @param string $methodClass
     *
     * @return void
     * @dataProvider executeProvider
     */
    public function testExecute(string $method, string $methodClass): void
    {

        $mock = $this->createMock($methodClass);
        $mock->expects($this->once())->method('sort')->willReturnArgument(0);
        Container::instance()->register($methodClass, $this->createConfiguredMock(
            ContainerCallbackInterface::class, ['execute' => $mock]
        ));
        self::assertFileDoesNotExist($this->out);

        $sorter = new Sorter($this->in, $this->out, $method);
        $sorter->execute();

        self::assertFileExists($this->out);
        self::assertFileEquals($this->out, $this->in);
    }
}