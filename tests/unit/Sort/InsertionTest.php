<?php

namespace Tests\Unit\Sort;

use App\Sort\Insertion;
use PHPUnit\Framework\TestCase;

class InsertionTest extends TestCase
{
    private function sortProvider(): array
    {
        return [
            [[0, -1, 10, 3, -9, 1, 18, 0], [-9, -1, 0, 0, 1, 3, 10, 18]]
        ];
    }

    /**
     * @param array $data
     * @param array $expected
     *
     * @return void
     * @dataProvider sortProvider
     */
    public function testSort(array $data, array $expected): void
    {
        $sort = new Insertion();
        self::assertEquals($expected, $sort->sort($data));
    }
}