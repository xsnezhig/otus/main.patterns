<?php

namespace Tests\Unit\Sort;

use App\Sort\Selection;
use PHPUnit\Framework\TestCase;

class SelectionTest extends TestCase
{
    private function sortProvider(): array
    {
        return [
            [[0, -1, 10, 3, -19, 8], [-19, -1, 0, 3, 8, 10]]
        ];
    }

    /**
     * @param array $data
     * @param array $expected
     *
     * @return void
     * @dataProvider sortProvider
     */
    public function testSort(array $data, array $expected): void
    {
        $sort = new Selection();
        self::assertEquals($expected, $sort->sort($data));
    }
}