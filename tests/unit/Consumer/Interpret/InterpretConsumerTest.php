<?php

namespace Tests\Unit\Consumer\Interpret;

use App\Commands\Interpret;
use App\Consumer\Interpret\InterpretConsumer;
use App\ContainerInterface;
use App\Interfaces\Command;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\TestCase;

class InterpretConsumerTest extends TestCase
{
    public function testExecute(): void
    {

        $gameId = random_int(10, 100);
        $objectId = random_int(101, 200);
        $operationId = 'random_operation_id';
        $operationArgs = ['foo' => random_int(1000, 10000)];

        $mockCommand = $this->createMock(Command::class);
        $mockCommand->expects($this->once())->method('execute');

        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockContainer->expects($this->once())
                      ->method('make')
                      ->with(Interpret::class, ['gameId' => $gameId, 'objectId' => $objectId, 'operationId' => $operationId, 'operationArgs' => $operationArgs])
                      ->willReturn($mockCommand);

        $mockMsg = $this->createMock(AMQPMessage::class);
        $mockMsg->body = json_encode(['gameId' => $gameId, 'objectId' => $objectId, 'operationId' => $operationId, 'operationArgs' => $operationArgs], JSON_THROW_ON_ERROR);

        $consumer = new InterpretConsumer($mockContainer);
        $consumer->execute($mockMsg);

    }
}