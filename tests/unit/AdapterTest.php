<?php

namespace Tests\Unit;

use App\Adapter;
use App\Container;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Classes\ClassA;
use Tests\Classes\ClassB;
use Tests\Interfaces\InterfaceForAdapter;

class AdapterTest extends TestCase
{
    private string $interface = InterfaceForAdapter::class;

    private static Container $containerOriginal;
    private static \ReflectionClass $reflection;
    private static object $obj;

    public static function setUpBeforeClass(): void
    {
        self::$reflection = new \ReflectionClass(Container::instance());
        self::$containerOriginal = Container::instance();
    }


    public function testCreate(): InterfaceForAdapter
    {
        self::$obj = $this->createMock(ClassA::class);
        $adapter = Container::instance()
                            ->make(Adapter::class, ['abstract' => $this->interface, 'concrete' => self::$obj]);
        $generated = $adapter->create();
        $this->assertInstanceOf($this->interface, $generated);
        return $generated;
    }

    /**
     * @depends testCreate
     */
    public function testGetSpecific(InterfaceForAdapter $generated): InterfaceForAdapter
    {
        $value = random_bytes(10);
        $this->mockContainer()
             ->expects(self::once())
             ->method('make')
             ->with($this->matchesRegularExpression('/^.*:specific\.get$/'), ['obj' => self::$obj, 'args' => []])
             ->willReturn($value);

        $this->assertEquals($value, $generated->getSpecific());

        return $generated;
    }

    /**
     * @depends testGetSpecific
     */
    public function testSetSpecific(InterfaceForAdapter $generated): InterfaceForAdapter
    {
        $value = 'value_test';
        $this->mockContainer()->method('make')
             ->with($this->matchesRegularExpression('/^.*:specific\.set$/'), ['obj' => self::$obj, 'args' => ['value' => $value]]);
        $generated->setSpecific($value);
        $this->addToAssertionCount(1);
        return $generated;
    }

    public function doSomethingComplicated(InterfaceForAdapter $generated): void
    {
        $incomingObj = $this->createMock(ClassB::class);
        $incomingAdapter = $this->createMock(InterfaceForAdapter::class);
        $expected = $this->createMock(InterfaceForAdapter::class);
        $this->mockContainer()
             ->method('make')
             ->with(
                 $this->matchesRegularExpression('/^.*:doSomethingComplicated$/'),
                 [
                     'obj'  => self::$obj,
                     'args' => ['obj' => $incomingObj, 'adapter' => $incomingAdapter]
                 ]
             )
             ->willReturn($expected);
        $actual = $generated->doSomethingComplicated($incomingObj, $incomingAdapter);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return MockObject<Container>
     * @throws \ReflectionException
     */
    private function mockContainer(): MockObject
    {
        $property = self::$reflection->getProperty('container');
        $property->setAccessible(true);
        $mock = $this->createMock(Container::class);
        $property->setValue($mock);
        return $mock;

    }

    public static function tearDownAfterClass(): void
    {
        $property = self::$reflection->getProperty('container');
        $property->setValue(self::$containerOriginal);
        $property->setAccessible(false);
    }

}