<?php

namespace Tests\Unit;

use App\Vector;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\ExpectedValues;
use PHPUnit\Framework\TestCase;

class VectorTest extends TestCase
{
    private function dataProvider(): array
    {
        return [
            [[0, 0], [1, 2], [1, 2]],
            [[0, 0], [-5, 7], [-5, 7]],
            [[5, 10], [1, -4], [6, 6]],
            [[-5, -10], [-1, 8], [-6, -2]],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testPlus(
        #[ArrayShape(['int', 'int'])]
        $base,
        #[ArrayShape(['int', 'int'])]
        $additional,
        #[ArrayShape(['int', 'int'])]
        $expect
    ): void {
        $vector = new Vector($base[0], $base[1]);
        $vector->plus(new Vector($additional[0], $additional[1]));
        self::assertEquals($expect[0], $vector->getX());
        self::assertEquals($expect[1], $vector->getY());
    }
}