import dotenv from 'dotenv';
import * as fs from "fs";
import path from "path";

const parsed = dotenv.config().parsed;
type config = {
    pk: string
}
const config: config = {
    pk: parsed?.AUTH_PK ? fs.readFileSync(path.resolve(process.cwd(), '.' + parsed.AUTH_PK)).toString() : ''
}

export default config;
