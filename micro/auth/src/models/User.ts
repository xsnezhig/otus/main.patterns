export type UserModelType = {
    get login(): string
    get password(): string
    set login(v: string);
    set password(v: string);
};
type UserType = {
    login: string,
    password: string
}
export default class User implements UserModelType {

    private data: { [K in keyof UserType]?: UserType[K] } = {};


    get login(): string {
        return this.data?.login || '';
    }

    get password(): string {
        return this.data?.password || '';
    }

    set login(v: string) {
        this.data.login = v;
    }

    set password(v: string) {
        this.data.password = v;
    }

}