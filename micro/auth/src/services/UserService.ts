import User, {UserModelType} from "../models/User";

export default class UserService {
    private users = [
        {login: 'foo', password: '123'},
        {login: 'bar', password: '321'}
    ];

    public getUser(login: string): UserModelType | null {
        for (const u of this.users) {
            if (u.login === login) {
                const user = new User();
                user.login = login;
                user.password = u.password;
                return user;
            }
        }
        return null;
    }
}