import UserService from "../services/UserService";
import {Request, Response} from "express";
import jwt from 'jsonwebtoken';
import config from "../../config";

export default class AuthController {

    public login(req: Request, res: Response) {
        const {login, password}: { login: string, password: string } = req.body;
        const user = (new UserService()).getUser(login);

        if (!user || password !== user.password) {
            return res.status(401).send({message: 'Invalid password or login'})
        }

        const token = jwt.sign({login}, config.pk, {expiresIn: 3600, algorithm: "RS256"});
        res
            .setHeader('Authorization', `Bearer ${token}`)
            .send({token});
    }
}