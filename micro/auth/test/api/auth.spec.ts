import {Server} from "http";

const request = require('supertest')
import app from "../../index";

describe('POST /auth', () => {
    let server: Server | null = null;
    beforeEach(() => server = app.listen(3000));
    afterEach(() => server?.close());

    it('Should return 200 with token', async () => {
        const res = await request(app)
            .post('/auth')
            .set('Accept', 'application/json')
            .send({login: 'foo', password: '123'});
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('token');
    })

    it('Should return 401', async () => {
        const res = await request(app)
            .post('/auth')
            .set('Accept', 'application/json')
            .send({login: 'foo', password: '1234'});
        expect(res.statusCode).toEqual(401);
    })
})