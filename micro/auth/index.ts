import express, {Express} from 'express';
import AuthController from "./src/controllers/AuthController";


const app: Express = express();
app.use(express.json());

app.post('/auth', (new AuthController()).login);

if(process.env.NODE_ENV !== 'test') {
    app.listen(3000);
}

export default app;