<?php

use App\Router;

require_once __DIR__ . '/../vendor/autoload.php';
Dotenv\Dotenv::createUnsafeImmutable(__DIR__ . '/../')->load();
try {
    (new Router())->run();
} catch (Throwable $e) {
    http_response_code(500);
    var_dump($e);
}
